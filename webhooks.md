# Webhooks

OurStreets supports configuring webhooks to recieve notifications when new
reports available to you are made by users. They are delivered to the URL you
configure your webhook for via an HTTP POST request.

## Headers

The webhook delivery includes the following special headers:

 * `X-OurStreets-Event` - The name of the event. Curently only `NEW_REPORT`
 * `X-OurStreets-Event-Id` - A UUID to identify the event payload
 * `X-OurStreets-Signature` - HMAC digest of the payload body using the configured secret as the key

## Body

The request body is a JSON encoded object with the following keys:
 * `address` - address found via geocod.io reverse geocoding from `location` field
 * `barcode` - for `vehicleType` in (`"BICYCLE"`, `"SCOOTER"`), the barcode value
 * `complete` - wether the report has been completed. for now always `true`
 * `createdAt` - iso8601 timestamp of when the report was submitted
 * `description` - user provided report description
 * `fleetId` - the fleet vehicle ID, such as scooter or bicycle id
 * `fleetOperator` - the fleet vehicle operator such as `Spin` or `JUMP`
 * `location` - GeoJSON Point object representing the location of the report
 * `number` - the license plate numbers/characters
 * `pictureUrl` - The URL of the photo if provided
 * `reportId` - The report ID, an integer
 * `reportTime` - iso8601 timestamp of when the reported incident occurred
 * `resolutionStatus` - `"new"`, `"in-progress"`, `"resolved"`, `"invalid"`, `"reopened"`
 * `resolutionReason` - `"operator"`, `"rented"`, `"self"`
 * `resovledNotes` - a string
 * `state` - the license plate state
 * `testing` - wheter the app was in test mode when submitting. always `false`
 * `tickets` - an array containgin the vehicle in the report's ticket history
 * `timezoneOffset` - the user's device's timezone offset at the time of submission
 * `total` - total value of citation history
 * `transportationMode` - For `violationType` = `"NEAR_MISS"` only, `"WALKING"`, `"BIKING"`, `"SCOOTING"`, or `"OTHER"`
 * `userId` - a UUID identifying an installation of OurStreets reporter
 * `vehicleMovement` - `PARKED`, `STANDING`, `LOADING`, or `MOVING`
 * `vehicleType` - The type of vehicle, eg `"SCOOTER"`, `"PRIVATE"`, `"UBER"`, etc
 * `violationType` - what the violation is, `IN_BIKE_LANE`, `ON_SIDEWALK`, etc

## Example delivery

```
POST /post HTTP/1.1
Host: httpbin.org
User-Agent: OurStreets/1.0
Content-Type: application/json
Content-Length: 671
X-Ourstreets-Event: NEW_REPORT
X-Ourstreets-Event-Id: cb6a7292-cb65-5e79-ab69-243a370d6aca
X-Ourstreets-Signature: 0076a1cfc8d421846cd3206ef5266a04
{"report_id": 3438, "state": null, "number": null, "html": null, "total": null, "created_at": "2020-01-27T14:56:13.177617Z", "vehicle_movement": "PARKED", "violation_type": "IN_BIKE_LANE", "description": null, "picture_url": null, "vehicle_type": "PRIVATE", "user_id": "f98119c6-af9c-4ab6-8a4c-33ed4e86616e", "timezone_offset": null, "location": {"type": "Point", "coordinates": [-76.9621162, 38.8679906]}, "address": "3167 Westover Dr SE, Washington, DC 20020", "complete": true, "report_time": "2020-01-27T14:56:11.639000Z", "transportation_mode": null, "barcode": null, "fleet_operator": null, "fleet_id": null, "resolved_status": null, "testing": null, "tickets": []}
```

## Verifying the signature
When configuring a webhook, you must provide a secret. this is use to create the
HMAC signature in the `X-OurStreets-Signature` header.

Here is an example of verifying the signature in python:
```python
import hmac, hashlib

# set secret to the actual value of your HMAC secret:
secret = ''
# you need to get the header value from the `X-Ourstreets-Signature` request
# header in your framework:
signature = '0076a1cfc8d421846cd3206ef5266a04'
# and similarly the body should be from your request as well:
body = '{"report_id": 3438, "state": null, "number": null, "html": null, "total": null, "created_at": "2020-01-27T14:56:13.177617Z", "vehicle_movement": "PARKED", "violation_type": "IN_BIKE_LANE", "description": null, "picture_url": null, "vehicle_type": "PRIVATE", "user_id": "f98119c6-af9c-4ab6-8a4c-33ed4e86616e", "timezone_offset": null, "location": {"type": "Point", "coordinates": [-76.9621162, 38.8679906]}, "address": "3167 Westover Dr SE, Washington, DC 20020", "complete": true, "report_time": "2020-01-27T14:56:11.639000Z", "transportation_mode": null, "barcode": null, "fleet_operator": null, "fleet_id": null, "resolved_status": null, "testing": null, "tickets": []}'

assert hmac.new(secret.encode(), body.encode(), digestmod=hashlib.md5).hexdigest() == signature
```
