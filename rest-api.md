# REST API

OurStreets supports configuring A REST API key to get and resolve
reports you have access to.

## Authentication
Add the access token you generated in OurStreets Manager/Fleet/Advocate as the `x-api-key` header.

## Report Object
Responses are JSON encoded, some are arrays of report objects some are report objects.
Report Objects have the following fields:
 * `address` - address found via geocod.io reverse geocoding from `location` field
 * `barcode` - for `vehicleType` in (`"BICYCLE"`, `"SCOOTER"`), the barcode value
 * `complete` - wether the report has been completed. for now always `true`
 * `createdAt` - iso8601 timestamp of when the report was submitted
 * `description` - user provided report description
 * `fleetId` - the fleet vehicle ID, such as scooter or bicycle id
 * `fleetOperator` - the fleet vehicle operator such as `Spin` or `JUMP`
 * `location` - GeoJSON Point object representing the location of the report
 * `number` - the license plate numbers/characters
 * `pictureUrl` - The URL of the photo if provided
 * `reportId` - The report ID, an integer
 * `reportTime` - iso8601 timestamp of when the reported incident occurred
 * `resolvedStatus` - `"new"`, `"in-progress"`,`"resolved"`, `"invalid"`, `"reopened"`
 * `resolutionReason` - `"operator"`, `"rented"`, `"self"`
 * `resolutionNotes` - a string
 * `state` - the license plate state
 * `testing` - wheter the app was in test mode when submitting. always `false`
 * `timezoneOffset` - the user's device's timezone offset at the time of submission
 * `total` - total value of citation history
 * `transportationMode` - For `violationType` = `"NEAR_MISS"` only, `"WALKING"`, `"BIKING"`, `"SCOOTING"`, or `"OTHER"`
 * `userId` - a UUID identifying an installation of OurStreets reporter
 * `vehicleMovement` - `PARKED`, `STANDING`, `LOADING`, or `MOVING`
 * `vehicleType` - The type of vehicle, eg `"SCOOTER"`, `"PRIVATE"`, `"UBER"`, etc
 * `violationType` - what the violation is, `IN_BIKE_LANE`, `ON_SIDEWALK`, etc

## Endpoints

### `GET https://api.ourstreets.com/rest/reports`
Get reports you have access to.
##### Query params
* `page` - pagination parameter
* `format` - `json`(default) or `geojson`
* `q` - a JSON encoded query filter. Example: <br>
`{"and":[{"name":"vehicle_type","op":"=","val":"LYFT"},{"name":"violation_type","op":"=","val":"IN_BIKE_LANE"}]}`

#### Example request & response

```
curl https://api.ourstreets.com/rest/reports -H 'x-api-key: YOUR_TOKEN'
{
    "results": [
      {
            "address": "2915 Pennsylvania Ave SE, Washington, DC 20020",
            "barcode": null,
            "complete": true,
            "createdAt": "2020-01-25T22:52:13.308654Z",
            "description": null,
            "fleetId": null,
            "fleetOperator": null,
            "location": {
                "coordinates": [
                    -76.96601394444444,
                    38.871405
                ],
                "type": "Point"
            },
            "number": null,
            "pictureUrl": "https://ourstreets-report-photos-dev.s3.amazonaws.com/35b378f6-ea4f-4b24-818e-c3a39e71a444",
            "reportId": 3347,
            "reportTime": "2020-01-25T19:02:01Z",
            "resolvedStatus": null,
            "state": null,
            "testing": false,
            "timezoneOffset": 5.0,
            "total": null,
            "transportationMode": null,
            "userId": "f98119c6-af9c-4ab6-8a4c-33ed4e86616e",
            "vehicleMovement": "MOVING",
            "vehicleType": "VIA",
            "violationType": "IN_BIKE_LANE"
        }
    ],
    "total": 1
}
```


### `GET https://api.ourstreets.com/rest/reports/:report_id`
Get a specific report by ID

#### Example request & response

```
curl https://api.ourstreets.com/rest/reports/1 -H 'x-api-key: YOUR_TOKEN'
{
    "address": "2915 Pennsylvania Ave SE, Washington, DC 20020",
    "barcode": null,
    "complete": true,
    "createdAt": "2020-01-25T22:52:13.308654Z",
    "description": null,
    "fleetId": null,
    "fleetOperator": null,
    "location": {
        "coordinates": [
            -76.96601394444444,
            38.871405
        ],
        "type": "Point"
    },
    "number": null,
    "pictureUrl": "https://ourstreets-report-photos-dev.s3.amazonaws.com/35b378f6-ea4f-4b24-818e-c3a39e71a444",
    "reportId": 3347,
    "reportTime": "2020-01-25T19:02:01Z",
    "resolvedStatus": null,
    "state": null,
    "testing": false,
    "tickets": [],
    "timezoneOffset": 5.0,
    "total": null,
    "transportationMode": null,
    "userId": "f98119c6-af9c-4ab6-8a4c-33ed4e86616e",
    "vehicleMovement": "MOVING",
    "vehicleType": "VIA",
    "violationType": "IN_BIKE_LANE"
}
```

### `PATCH https://api.ourstreets.com/rest/reports/:report_id`
Update a specific report by ID. You can only update `resolvedStatus`, `resolutionReason`, and `resolutionNotes`.

#### Example request & response

```
curl https://api.ourstreets.com/rest/reports/1 -H 'x-api-key: YOUR_TOKEN' -X PATCH --data-binary '{"resolvedStatus": "disputed"}'
{
      "address": "2915 Pennsylvania Ave SE, Washington, DC 20020",
      "barcode": null,
      "complete": true,
      "createdAt": "2020-01-25T22:52:13.308654Z",
      "description": null,
      "fleetId": null,
      "fleetOperator": null,
      "location": {
          "coordinates": [
              -76.96601394444444,
              38.871405
          ],
          "type": "Point"
      },
      "number": null,
      "pictureUrl": "https://ourstreets-report-photos-dev.s3.amazonaws.com/35b378f6-ea4f-4b24-818e-c3a39e71a444",
      "reportId": 3347,
      "reportTime": "2020-01-25T19:02:01Z",
      "resolvedStatus": "disputed",
      "state": null,
      "testing": false,
      "timezoneOffset": 5.0,
      "total": null,
      "transportationMode": null,
      "userId": "f98119c6-af9c-4ab6-8a4c-33ed4e86616e",
      "vehicleMovement": "MOVING",
      "vehicleType": "VIA",
      "violationType": "IN_BIKE_LANE"
  }
```
